import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//importación de páginas
import { MainComponent } from './pages/main/main.component';
import{ HomeComponent} from './pages/home/home.component';
import {ConvertionComponent } from './pages/convertion/convertion.component';
import {GraphsComponent } from './pages/graphs/graphs.component';
import { HistoricalComponent} from './pages/historical/historical.component';

const routes: Routes = [
  {
    path: 'base',
    component: MainComponent,
    children: [
      {
        path: 'home',
        component: HomeComponent
      },{
        path: 'conversions',
        component: ConvertionComponent
      }, {
        path: 'graphs',
        component: GraphsComponent
      },{
        path: 'records',
        component: HistoricalComponent 
      },
    ]
  },{
    path:'',
    redirectTo:'base',
    pathMatch:'full',
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
