//Importaciones de angular
import { Injectable } from '@angular/core';

//Importación de componentes HTTP
 import { HttpClient } from '@angular/common/http';

// Importación de constantes
import * as constants from '../../../environments/environment';

//Importación de operadores de la librería RxJS
import { map, timeout } from 'rxjs/operators';
import { from } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DivisasService {

  private TIMEOUT = 5000;

  constructor( private http: HttpClient) { }

  public getMoneyByCountry( country: string ) {

    return this.http.get( constants.URL_BASE + constants.END_POINT.GET_MONEY_BY_COUNTRY + country).pipe(
      timeout(this.TIMEOUT)
    ).pipe(
      map( response => {
        return response;
      })
    );
  }
}
