import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilitiesService {

  constructor() { }

  public truncNumber(numToBeTruncated: any, numOfDecimals: any) {
    const num = numToBeTruncated.toString()
    const position = num.indexOf('.');

    return +( num.slice(0, position > -1 ? ++numOfDecimals + position : undefined) );
  }
}
