import { Component, OnInit } from '@angular/core';


//Importación de constantes
import * as General from '../../constants/general';

//Importación de providers
import {DivisasService} from '../../providers/divisas/divisas.service';
import {UtilitiesService} from '../../providers/utilities/utilities.service';


import { from } from 'rxjs';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  //Fecha
  private dateGeneral = new Date ();
  private day: number;
  private month: number;
  private year: number;
  public dateToday: string;

  //Imagenes
  public flagUSA = General.FLAG_USA;

  //Variable con tipo de cambio
  public priceDollarToday: number;

  constructor( private divisas: DivisasService,
              private utilities: UtilitiesService ) { 
    this.day = this.dateGeneral.getDate();
    this.month = this.dateGeneral.getMonth() +1;
    this.year = this.dateGeneral.getFullYear();

    this.dateToday = this.day.toString() + "/" + this.month.toString() + "/" + this.year.toString();

    this.getDivisasByCountry();
    console.log(this.dateToday);
  }

  ngOnInit(): void {
  }


  private getDivisasByCountry() {
    const country = 'USD'

    this.divisas.getMoneyByCountry(country).subscribe( (dataResponse: any) => {
      try {
        this.priceDollarToday = this.utilities.truncNumber(dataResponse.rates.MXN,2)
        
      }catch(err) {
          console.log('Falló el servicio');
        }
      
    })
  }
}
