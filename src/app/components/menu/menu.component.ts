import { Component, OnInit } from '@angular/core';

//importación de servicios para traducción
import {TranslateService} from '@ngx-translate/core'

//importación de constantes
import * as General from '../../constants/general';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  private activeEs = 'es';

  //images
  public logo = General.LOGO; 
  public iconHome = General.HOME_ICON;
  public iconConvertion = General.CONVERT_ICON;
  public iconGraphs = General.GRAPHS_ICON;
  public iconRecords = General.RECORDS_ICON;


  constructor(private translate: TranslateService ) {

    //Activación del lenguaje
    this. translate.setDefaultLang(this.activeEs)
   }

  ngOnInit(): void {
  }

}
