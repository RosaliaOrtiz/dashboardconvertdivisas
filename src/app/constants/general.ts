export const LOGO = '../../assets/icons/logo.png';

//Icons
export const HOME_ICON = '../../assets/icons/home.png';
export const CONVERT_ICON = '../../assets/icons/convertir.png';
export const GRAPHS_ICON = '../../assets/icons/grafica.png';
export const RECORDS_ICON = '../../assets/icons/history.png';

export const FLAG_USA = '../../assets/icons/united-states.png';
export const FLAG_MEXICO = '../../assets/icons/mexico.png';